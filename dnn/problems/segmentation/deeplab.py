# -*- coding: utf-8 -*-

""" Deeplabv3+ model for Keras.
This model is based on the example:
https://keras.io/examples/vision/deeplabv3_plus/
# Reference
- [Encoder-Decoder with Atrous Separable Convolution
    for Semantic Image Segmentation](https://arxiv.org/pdf/1802.02611.pdf)
- [Inverted Residuals and Linear Bottlenecks: Mobile Networks for
    Classification, Detection and Segmentation](https://arxiv.org/abs/1801.04381)
"""

from os import environ
environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

from tensorflow.keras.applications import MobileNetV2
from tensorflow.keras import initializers
from tensorflow.keras import regularizers
from tensorflow.keras import backend as K
from tensorflow.keras import layers
from tensorflow.keras import Model
from tensorflow.keras import Input
import tensorflow as tf
import numpy as np

class Losses_n_Metrics():
    def __init__(self, ignore_last=True):
        self.ignore_last = ignore_last

    def dice_coef(self, y_true, y_pred, smooth=1):
        """ Dice loss - Ignore last class from true mask
            args:
                y_true: ground truth 4D keras tensor (B,H,W,C)
                y_pred: predicted 4D keras tensor (B,H,W,C)
                smooth: value to avoid division by zero
            return:
                dice loss
        """
        # nb_classes = K.int_shape(y_pred)[-1]

        # y_true_f = K.one_hot(
        #     tf.cast(y_true[:, :, :, 0], tf.int32), nb_classes + 1)

        # if self.ignore_last:
        #     y_true_f = y_true_f[:, :, :, :-1]

        y_true_f = K.batch_flatten(y_true)
        y_pred_f = K.batch_flatten(y_pred)

        intersection = 2. * K.sum(y_true_f * y_pred_f, axis=-1) + smooth
        union = K.sum(y_true_f, axis=-1) + K.sum(y_pred_f, axis=-1) + smooth

        return intersection / union

    def loss_dice_coef(self, y_true, y_pred):
        """ Dice loss
            args:
                y_true: ground truth 4D keras tensor (B,H,W,C)
                y_pred: predicted 4D keras tensor (B,H,W,C)
            return:
                dice loss
        """
        return 1 - self.dice_coef(y_true, y_pred)

    def iou_coef(self, y_true, y_pred, smooth=1):
        # nb_classes = K.int_shape(y_pred)[-1]

        # y_true_f = K.one_hot(
        #     tf.cast(y_true[:, :, :, 0], tf.int32), nb_classes + 1)

        # if self.ignore_last:
        #     y_true_f = y_true_f[:, :, :, :-1]

        y_true_f = K.batch_flatten(y_true)
        y_pred_f = K.batch_flatten(y_pred)

        intersection = K.sum(K.abs(y_true_f * y_pred_f), axis=-1)
        union = K.sum(y_true_f, axis=-1) + K.sum(y_pred_f, axis=-1) - intersection
        iou = K.mean((intersection + smooth) / (union + smooth), axis=0)
        return iou

    def loss_iou_coef(self, y_true, y_pred):
        return 1 - self.iou_coef(y_true, y_pred)

    def accuracy(self, y_true, y_pred):
        """ Accuracy = True Positive + True Negative / (True Positive + False Positive + True Negative + False Negative)
            args:
                y_true: ground truth 4D keras tensor (B,H,W,C)
                y_pred: predicted 4D keras tensor (B,H,W,C)
                return:
                    accuracy
        """
        y_pred_numpy = np.argmax(y_pred.numpy(), axis=-1)
        y_true_numpy = y_true[:, :, 0].numpy()

        batch_size = y_pred_numpy.shape[0]
        accuracy_ = 0

        for i in range(batch_size):
            # True positive 1 & 1
            TP = np.bitwise_and(
                (y_true_numpy[i] == 1), (y_pred_numpy[i] == 1)).sum()
            # False positive 0 & 1
            FP = np.bitwise_and(
                (y_true_numpy[i] == 0), (y_pred_numpy[i] == 1)).sum()
            # True negative 0 & 0
            TN = np.bitwise_and(
                (y_true_numpy[i] == 0), (y_pred_numpy[i] == 0)).sum()
            # False negative 1 & 0
            FN = np.bitwise_and(
                (y_true_numpy[i] == 1), (y_pred_numpy[i] == 0)).sum()

            accuracy_ += (TP + TN) / (TP + FP + TN + FN + K.epsilon())

        return accuracy_ / batch_size


    def precision(self, y_true, y_pred):
        """ Precision = True Positive / (True Positive + False Positive)))
            args:
                y_true: ground truth 4D keras tensor (B,H,W,C)
                y_pred: predicted 4D keras tensor (B,H,W,C)
            return:
                precision
        """
        y_pred_numpy = np.argmax(y_pred.numpy(), axis=-1)
        y_true_numpy = y_true[:, :, 0].numpy()

        batch_size = y_pred_numpy.shape[0]
        precision_ = 0

        for i in range(batch_size):
            # True positive 1 & 1
            TP = np.bitwise_and(
                (y_true_numpy[i] == 1), (y_pred_numpy[i] == 1)).sum()
            # False positive 0 & 1
            FP = np.bitwise_and(
                (y_true_numpy[i] == 0), (y_pred_numpy[i] == 1)).sum()

            precision_ += (TP) / (TP + FP + K.epsilon())

        return precision_ / batch_size


    def recall(self, y_true, y_pred):
        """ Recall =  True Positive / (True Positive + False Negative)
            args:
                y_true: ground truth 4D keras tensor (B,H,W,C)
                y_pred: predicted 4D keras tensor (B,H,W,C)
            return:
                recall
        """
        y_pred_numpy = np.argmax(y_pred.numpy(), axis=-1)
        y_true_numpy = y_true[:, :, 0].numpy()

        batch_size = y_pred_numpy.shape[0]
        recall_ = 0

        for i in range(batch_size):
            # True positive - 1 & 1
            TP = np.bitwise_and(
                (y_true_numpy[i] == 1), (y_pred_numpy[i] == 1)).sum()
            # False negative - 1 & 0
            FN = np.bitwise_and(
                (y_true_numpy[i] == 1), (y_pred_numpy[i] == 0)).sum()

            recall_ += (TP) / (TP + FN + K.epsilon())

        return recall_ / batch_size


    def f1Score(self, y_true, y_pred):
        """ F1 Score = 2 * Precision * Recall / (Precision + Recall)
            args:
                y_true: ground truth 4D keras tensor (B,H,W,C)
                y_pred: predicted 4D keras tensor (B,H,W,C)
            return:
                f1 score
        """
        y_pred_numpy = np.argmax(y_pred.numpy(), axis=-1)
        y_true_numpy = y_true[:, :, 0].numpy()

        batch_size = y_pred_numpy.shape[0]

        precision_ = np.zeros(batch_size)
        recall_ = np.zeros(batch_size)

        for i in range(batch_size):
            TP = np.bitwise_and(
                (y_true_numpy[i] == 1), (y_pred_numpy[i] == 1)).sum()
            FP = np.bitwise_and(
                (y_true_numpy[i] == 0), (y_pred_numpy[i] == 1)).sum()
            FN = np.bitwise_and(
                (y_true_numpy[i] == 1), (y_pred_numpy[i] == 0)).sum()

            precision_[i] = (TP) / (TP + FP + K.epsilon())
            recall_[i] = (TP) / (TP + FN + K.epsilon())

        f1Score_ = 2 * ((precision_ * recall_) /
                        (precision_ + recall_ + K.epsilon()))

        return f1Score_.mean()


    def mIOU(self, y_true, y_pred):
        """ Mean Intersection over Union
            args:
                y_true: ground truth 4D keras tensor (B,H,W,C)
                y_pred: predicted 4D keras tensor (B,H,W,C)
            return:
                mIOU
        """
        y_pred_numpy = np.argmax(y_pred.numpy(), axis=-1)
        y_true_numpy = y_true[:, :, 0].numpy()

        batch_size = y_pred_numpy.shape[0]
        ulabels = np.unique(y_true_numpy).astype(np.uint8)
        iou = np.zeros(batch_size)

        for i in range(batch_size):
            iou_temp = np.zeros(len(ulabels))

            for k, u in enumerate(ulabels):
                inter = (y_true_numpy == u) & (y_pred_numpy == u)
                union = (y_true_numpy == u) | (y_pred_numpy == u)

                iou_temp[k] = inter.sum() / union.sum()

            iou[i] = iou_temp.mean()

        return iou.mean()


def deeplabV3(imageSize=(256, 256), nClasses=20, nChannel=3, alpha=1., withArgmax=False,
              mobileLayers=None, kernel_regularizer=None, SEED=None,
              kernel_initializer=None, frozen=False,  TEST=False):
    """ Returns a model with MobineNetv2 backbone encoder and a DeeplabV3Plus decoder.
        Args:
            imageSize - int - Input image size (imageSize, imageSize, channels)
            nClasses - int - Number of classes in output
            alpha - float - Alpha value for the MobileNetV2 backbone
            withArgmax - bool - If True, the model will output the argmax(imageSize, imageSize, classes), otherwise the reshaped output(imageSize * imageSize, nClasses)
    """
    if kernel_initializer is None:
        kernel_initializer = initializers.RandomUniform(minval=-2.5, maxval=2.5, seed=SEED)

    if mobileLayers is None:
        mobileLayers = {"shallowLayer": "block_2_project_BN",
                        "deepLayer": "block_12_project_BN"}

    def convolution_block(block_input, num_filters=256, kernel_size=3,
                          dilation_rate=1, padding="same", use_bias=False,
                          activation=True, activation_type_in="relu", name=""):
        """ Returns a convolution block with the following structure:
        Args:
            block_input - Input tensor
            num_filters - int - Number of filters in the convolution
            kernel_size - int - Kernel size of the convolution
            dilation_rate - int - Dilation rate of the convolution
            padding - str - Padding of the convolution
            use_bias - bool - If True, a bias will be added to the convolution
            activity_regularizer - Regularizer function applied to the activity
            activation - bool - If True, a relu activation will be applied
            name - str - Name of the block
        Returns:
            x - Output tensor
        """
        x = layers.Conv2D(
            num_filters,
            kernel_size=kernel_size,
            dilation_rate=dilation_rate,
            padding="same",
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            name=name + "_Conv2D"
        )(block_input)

        x = layers.BatchNormalization(epsilon=0.00001, name=name + "_BN")(x)

        if activation:
            x = layers.Activation(activation_type_in, name=name + "_ReLU")(x)

        return x

    def SeparableConv2D_block(block_input, num_filters=256, kernel_size=3,
                              dilation_rate=1, padding="same", use_bias=False,
                              activation=True, activation_type_in="relu", name=""):
        """ Returns a convolution block with the following structure:
        Args:
            block_input - Input tensor
            num_filters - int - Number of filters in the convolution
            kernel_size - int - Kernel size of the convolution
            dilation_rate - int - Dilation rate of the convolution
            padding - str - Padding of the convolution
            use_bias - bool - If True, a bias will be added to the convolution
            activity_regularizer - Regularizer function applied to the activity
            activation - bool - If True, a relu activation will be applied
            name - str - Name of the block
        Returns:
            x - Output tensor
        """
        x = layers.SeparableConv2D(
            num_filters,
            kernel_size=kernel_size,
            dilation_rate=dilation_rate,
            padding="same",
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            name=name + "_SeparableConv2D"
        )(block_input)

        x = layers.BatchNormalization(epsilon=0.00001, name=name + "_BN")(x)

        if activation:
            x = layers.Activation(activation_type_in, name=name + "_ReLU")(x)

        return x


    def ASPP(aspp_input):
        """ Returns an ASPP block with the following structure:
            Args:
                aspp_input - Input tensor
            Returns:
                x - Output tensor
        """
        dims = aspp_input.shape
        x = layers.AveragePooling2D(pool_size=(dims[-3], dims[-2]),
                                    name="assp_pool")(aspp_input)
        x = convolution_block(x, kernel_size=1, use_bias=True,
                              name="aspp_pool")

        out_pool = layers.UpSampling2D(size=(dims[-3] // x.shape[1],
                                             dims[-2] // x.shape[2]),
            interpolation="bilinear", name="aspp_pool_UpSampling")(x)

        out_1 = convolution_block(
            aspp_input, kernel_size=1, dilation_rate=1, name="aspp_out1")

        x = layers.Concatenate(axis=-1, name="aspp_Concat")([out_pool, out_1])

        aspp_output = convolution_block(x, kernel_size=1, name="aspp_out")

        return layers.Dropout(.25, name="aspp_Dropout")(aspp_output)

    def DeeplabV3PlusMobileNetv2(imageSize, nClasses, nChannel = 3):
        """ Returns a DeeplabV3Plus model with the following structure:
            Args:
                imageSize - int - Input image size (imageSize, imageSize, channels)
                nClasses - int - Number of classes in output
            Returns:
                model - Keras model
        """
        model_input = Input(shape=(imageSize[0], imageSize[1], nChannel))
        model_input = layers.Rescaling(scale=1./127.5, offset=-1)(model_input)
        mobilenetv2 = MobileNetV2(weights=None, include_top=False,
                                  input_tensor=model_input, alpha=alpha)

        if frozen:
            for layer in mobilenetv2.layers:
                layer.trainable = False

        input_a = ASPP(mobilenetv2.get_layer(mobileLayers["deepLayer"]).output)
        input_b = mobilenetv2.get_layer(mobileLayers["shallowLayer"]).output

        input_a = layers.UpSampling2D(size = (4, 4),
                                      interpolation="bilinear",
                                      name="input_a_UpSampling")(input_a)

        input_b = convolution_block(input_b, num_filters=48, kernel_size=1,
                                    name="input_b")

        x = layers.Concatenate(axis=-1, name="features_Concat")([input_a, input_b])

        x = SeparableConv2D_block(x, kernel_size=3, name="output_")

        if TEST:
            x = SeparableConv2D_block(x, nClasses, kernel_size=3, name="output2_")
        else:
            x = layers.Conv2D(nClasses, kernel_size=1, padding="same",
                              kernel_regularizer=kernel_regularizer,
                          name="nClasses_Conv2D")(x)

        x = layers.UpSampling2D(size = (4, 4), interpolation="bilinear",
                                name="output_UpSampling")(x)

        if TEST:
            x = layers.SeparableConv2D(
                nClasses, 1,
                padding="same",
                kernel_initializer=kernel_initializer,
                kernel_regularizer=kernel_regularizer,
                name="last_SeparableConv2D")(x)

        x = layers.Activation(activation='softmax', name="softmax")(x)

        if withArgmax:
            x = layers.Lambda(lambda x : K.argmax(x, axis=-1), name="argmax")(x)
        else:
            x = layers.Reshape((x.shape[1], x.shape[2], x.shape[3]),
                               name="output")(x)

        return Model(inputs=model_input, outputs=x, name="DeepLabV3Plus")

    if isinstance(imageSize, int):
        imageSize = (imageSize, imageSize)

    return DeeplabV3PlusMobileNetv2(imageSize=imageSize, nClasses=nClasses, nChannel=nChannel)

def deeplabV3Alpha(imageSize=(256, 256), nClasses=20, alpha=1., last=0.25, withArgmax=False,
              mobileLayers=None, kernel_regularizer=None, SEED=None, frozen=False):
    """ Returns a model with MobineNetv2 backbone encoder and a DeeplabV3Plus decoder.
        Args:
            imageSize - int - Input image size (imageSize, imageSize, channels)
            nClasses - int - Number of classes in output
            alpha - float - Alpha value for the MobileNetV2 backbone
            withArgmax - bool - If True, the model will output the argmax(imageSize, imageSize, classes), otherwise the reshaped output(imageSize * imageSize, nClasses)
    """

    kernel_initializer = initializers.RandomUniform(minval=-2.5, maxval=2.5, seed=SEED)

    if mobileLayers is None:
        mobileLayers = {"shallowLayer": "block_2_project_BN",
                        "deepLayer": "block_12_project_BN"}

    def SeparableConv2D_block(block_input, num_filters=int(256 * alpha), kernel_size=3,
                              dilation_rate=1, padding="same", use_bias=False,
                              activation=True, activation_type_in="relu", name=""):
        """ Returns a convolution block with the following structure:
        Args:
            block_input - Input tensor
            num_filters - int - Number of filters in the convolution
            kernel_size - int - Kernel size of the convolution
            dilation_rate - int - Dilation rate of the convolution
            padding - str - Padding of the convolution
            use_bias - bool - If True, a bias will be added to the convolution
            activity_regularizer - Regularizer function applied to the activity
            activation - bool - If True, a relu activation will be applied
            name - str - Name of the block
        Returns:
            x - Output tensor
        """
        x = layers.SeparableConv2D(
            num_filters,
            kernel_size=kernel_size,
            dilation_rate=dilation_rate,
            padding="same",
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            name=name + "_SeparableConv2D"
        )(block_input)

        x = layers.BatchNormalization(epsilon=0.00001, name=name + "_BN")(x)

        if activation:
            x = layers.Activation(activation_type_in, name=name + "_ReLU")(x)

        return x

    def ASPP(aspp_input):
        """ Returns an ASPP block with the following structure:
            Args:
                aspp_input - Input tensor
            Returns:
                x - Output tensor
        """
        dims = aspp_input.shape
        x = layers.AveragePooling2D(pool_size=(dims[-3], dims[-2]),
                                    name="assp_pool")(aspp_input)
        x = SeparableConv2D_block(x, kernel_size=1, use_bias=True,
                              name="aspp_pool")

        out_pool = layers.UpSampling2D(size=(dims[-3] // x.shape[1],
                                             dims[-2] // x.shape[2]),
            interpolation="bilinear", name="aspp_pool_UpSampling")(x)

        out_1 = SeparableConv2D_block(
            aspp_input, kernel_size=1, dilation_rate=1, name="aspp_out1")

        x = layers.Concatenate(axis=-1, name="aspp_Concat")([out_pool, out_1])

        aspp_output = SeparableConv2D_block(x, kernel_size=1, name="aspp_out")

        return layers.Dropout(.25, name="aspp_Dropout")(aspp_output)

    def DeeplabV3PlusMobileNetv2(imageSize, nClasses, nChannel = 3):
        """ Returns a DeeplabV3Plus model with the following structure:
            Args:
                imageSize - int - Input image size (imageSize, imageSize, channels)
                nClasses - int - Number of classes in output
            Returns:
                model - Keras model
        """
        model_input = Input(shape=(imageSize[0], imageSize[1], nChannel))
        model_input = layers.Rescaling(scale=1./127.5, offset=-1)(model_input)
        mobilenetv2 = MobileNetV2(weights="imagenet", include_top=False,
                                  input_tensor=model_input, alpha=alpha)

        if frozen:
            for layer in mobilenetv2.layers:
                layer.trainable = False

        input_a = ASPP(mobilenetv2.get_layer(mobileLayers["deepLayer"]).output)
        input_b = mobilenetv2.get_layer(mobileLayers["shallowLayer"]).output

        input_a = layers.UpSampling2D(size = (4, 4),
                                      interpolation="bilinear",
                                      name="input_a_UpSampling")(input_a)

        input_b = SeparableConv2D_block(input_b, num_filters=int(48 * alpha),
                                        kernel_size=1, name="input_b")

        x = layers.Concatenate(axis=-1, name="features_Concat")([input_a, input_b])

        x = SeparableConv2D_block(x, kernel_size=3, name="output1")

        x = layers.Dropout(.25, name="output_Dropout")(x)

        x = SeparableConv2D_block(x, num_filters=int(256 * alpha * last),
                                  kernel_size=3, name="output2")

        x = layers.UpSampling2D(size = (4, 4), interpolation="bilinear",
                                name="output_UpSampling")(x)

        x = layers.SeparableConv2D(
            nClasses, 1,
            padding="same",
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            name="last_SeparableConv2D")(x)

        x = layers.Activation(activation='softmax', name="softmax")(x)

        if withArgmax:
            x = layers.Lambda(lambda x : K.argmax(x, axis=-1), name="argmax")(x)
        else:
            x = layers.Reshape((x.shape[1], x.shape[2], x.shape[3]),
                               name="output")(x)

        return Model(inputs=model_input, outputs=x, name="DeepLabV3Plus")

    if isinstance(imageSize, int):
        imageSize = (imageSize, imageSize)

    return DeeplabV3PlusMobileNetv2(imageSize=imageSize, nClasses=nClasses)


def deeplabV3Alpha2(imageSize=(256, 256), nClasses=20, alpha=1., last=0.25, withArgmax=False,
              mobileLayers=None, kernel_regularizer=None, SEED=None, frozen=False):
    """ Returns a model with MobineNetv2 backbone encoder and a DeeplabV3Plus decoder.
        Args:
            imageSize - int - Input image size (imageSize, imageSize, channels)
            nClasses - int - Number of classes in output
            alpha - float - Alpha value for the MobileNetV2 backbone
    """

    kernel_initializer = initializers.RandomUniform(minval=-2.5, maxval=2.5, seed=SEED)

    if mobileLayers is None:
        mobileLayers = {
            "shallowLayer": "block_2_project_BN", "deepLayer": "block_12_project_BN"}

    def SeparableConv2D_block(
            block_input, num_filters=int(256 * alpha * 0.5), kernel_size=3,
            dilation_rate=1, padding="same", use_bias=False, activation=True,
            activation_type_in="relu", name=""):
        """ Returns a convolution block with the following structure:
        Args:
            block_input - Input tensor
            num_filters - int - Number of filters in the convolution
            kernel_size - int - Kernel size of the convolution
            dilation_rate - int - Dilation rate of the convolution
            padding - str - Padding of the convolution
            use_bias - bool - If True, a bias will be added to the convolution
            activity_regularizer - Regularizer function applied to the activity
            activation - bool - If True, a relu activation will be applied
            name - str - Name of the block
        Returns:
            x - Output tensor
        """
        x = layers.SeparableConv2D(
            num_filters,
            kernel_size=kernel_size,
            dilation_rate=dilation_rate,
            padding="same",
            use_bias=use_bias,
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            name=name + "_SeparableConv2D"
        )(block_input)

        x = layers.BatchNormalization(epsilon=0.00001, name=name + "_BN")(x)

        if activation:
            x = layers.Activation(activation_type_in, name=name + "_ReLU")(x)

        return x

    def ASPP(aspp_input):
        """ Returns an ASPP block with the following structure:
            Args:
                aspp_input - Input tensor
            Returns:
                x - Output tensor
        """
        dims = aspp_input.shape
        x = layers.AveragePooling2D(
            pool_size=(dims[-3], dims[-2]), name="assp_pool")(aspp_input)
        x = SeparableConv2D_block(
            x, kernel_size=1, use_bias=True, name="aspp_pool")

        out_pool = layers.UpSampling2D(
            size=(dims[-3] // x.shape[1], dims[-2] // x.shape[2]),
            interpolation="bilinear", name="aspp_pool_UpSampling")(x)

        out_1 = SeparableConv2D_block(
            aspp_input, kernel_size=1, dilation_rate=1, name="aspp_out1")

        x = layers.Concatenate(axis=-1, name="aspp_Concat")([out_pool, out_1])

        aspp_output = SeparableConv2D_block(x, kernel_size=1, name="aspp_out")

        return layers.Dropout(.25, name="aspp_Dropout")(aspp_output)

    def DeeplabV3PlusMobileNetv2(imageSize, nClasses, nChannel = 3):
        """ Returns a DeeplabV3Plus model with the following structure:
            Args:
                imageSize - int - Input image size (imageSize, imageSize, channels)
                nClasses - int - Number of classes in output
            Returns:
                model - Keras model
        """
        model_input = Input(shape=(imageSize[0], imageSize[1], nChannel))
        model_input = layers.Rescaling(scale=1./127.5, offset=-1)(model_input)

        mobilenetv2 = MobileNetV2(
            weights="imagenet", include_top=False, input_tensor=model_input, alpha=alpha)

        input_a = ASPP(mobilenetv2.get_layer(mobileLayers["deepLayer"]).output)
        input_b = mobilenetv2.get_layer(mobileLayers["shallowLayer"]).output

        input_a = layers.UpSampling2D(
            size=(4, 4), interpolation="bilinear", name="input_a_UpSampling")(input_a)

        input_b = SeparableConv2D_block(
            input_b, num_filters=int(48 * alpha), kernel_size=1, name="input_b")

        x = layers.Concatenate(axis=-1, name="features_Concat")([input_a, input_b])

        x = SeparableConv2D_block(x, kernel_size=3, name="output1")
        x = layers.Dropout(.25, name="output_Dropout")(x)
        x = SeparableConv2D_block(
            x, num_filters=int(256 * alpha * 0.25), kernel_size=3, name="output2")

        x = layers.UpSampling2D(
            size=(4, 4), interpolation="bilinear", name="output_UpSampling")(x)

        x = layers.SeparableConv2D(
            nClasses,
            kernel_size=1,
            padding="same",
            kernel_initializer=kernel_initializer,
            kernel_regularizer=kernel_regularizer,
            name="last_SeparableConv2D")(x)

        x = layers.Activation(activation='softmax', name="softmax")(x)

        x = layers.Reshape((x.shape[1] * x.shape[2], x.shape[3]), name="output")(x)

        return Model(inputs=model_input, outputs=x, name="DeepLabV3Plus")

    if isinstance(imageSize, int):
        imageSize = (imageSize, imageSize)

    return DeeplabV3PlusMobileNetv2(imageSize=imageSize, nClasses=nClasses, nChannel = nChannel)


def build_mobilenetv2_unet (input_shape, n_classes = 1, trainable = False):
  image_size = input_shape [:2]
  n_channel = input_shape [-1]
  return deeplabV3 (
    image_size, n_classes, nChannel = n_channel,
    frozen = not trainable,
    alpha = 0.5,
    kernel_regularizer=tf.keras.regularizers.L1L2 (l1 = 1e-6, l2 = 1e-4)
  )


if __name__ == '__main__':
  model = build_mobilenetv2_unet ((128, 128, 1), 1, trainable = True)
  model.summary ()